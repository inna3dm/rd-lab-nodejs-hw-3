const { User } = require('../models/userModel');
const { Load } = require('../models/loadModel');

const addLoadForUser = async (data, user) => {
  const load = new Load({...data, created_by: user._id});
  await load.save();
};

const getLoadByIdForUser = async (loadId, user) => {
  const load = await Load.findOne({_id: loadId, created_by: user._id});
  return { load };
};

const getLoadAssignedToUser = async (user) => {
  const load = await Load.findOne({assigned_to: user._id});
  return { load };
};

const updateLoadStateAssignedToUser = async (user) => {
  const load = await Load.findOne({assigned_to: user._id});

  if (!load) {
    throw new Error(`You don't have assigned load`);
  }
  const prevStatus = load.status;

  let status;
  if (prevStatus === 'NEW') {
    status = 'POSTED';
  } else if(prevStatus === 'POSTED') {
    status = 'ASSIGNED';
  } else if(prevStatus === 'ASSIGNED') {
    status = 'SHIPPED';
  } else {
    status = 'NEW';
  }

  await Load.updateOne(load, {$set: {status}});
  return { status };
};

const updateLoadByIdForUser = async (loadId, user, data) => {
  const load = await Load.findOne({ id: loadId, created_by: user._id, status: 'NEW' });
  if (!load) {
    throw new Error(`You can't update load, it status is in progress`);
  }
  await Load.updateOne(load, {$set: {data}});
};

const deleteLoadByIdForUser = async (loadId, user) => {
  const load = await Load.findOne({ id: loadId, created_by: user._id, status: 'NEW' });
  if (!load) {
    throw new Error(`You can't update load, it status is in progress`);
  }
  await Load.deleteOne({id: loadId, created_by: user._id});
};

module.exports = {
  addLoadForUser,
  getLoadByIdForUser,
  getLoadAssignedToUser,
  updateLoadStateAssignedToUser,
  updateLoadByIdForUser,
  deleteLoadByIdForUser,
};
