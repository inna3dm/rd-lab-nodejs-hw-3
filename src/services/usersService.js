const { User } = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserProfile = async (userId) => {
  const user = await User.findOne({_id: userId}).select('-password');
  return user;
};

const changeUserPassword = async (userId, data) => {
  const user = await User.findOne({_id: userId});

  if (!(await bcrypt.compare(data.oldPassword, user.password))) {
    throw new Error('Old password is incorrect');
  }
  const password = await bcrypt.hash(data.newPassword, 10);
  await User.updateOne(user, {$set: {password}});
};

const deleteUserProfile = async (userId) => {
  await User.findOneAndRemove({_id: userId});
};

module.exports = {
  getUserProfile,
  changeUserPassword,
  deleteUserProfile,
};
