const { User } = require('../models/userModel');
const { Truck } = require('../models/truckModel');

const getTrucksByUserId = async (user) => {
  const trucks = await Truck.find({created_by: user._id});
  return { trucks };
};

const getTruckByIdForUser = async (truckId, user) => {
  return await Truck.findOne({_id: truckId, created_by: user._id});
};

const addTruckForUser = async (data, user) => {
  const truck = new Truck({...data, created_by: user._id});
  await truck.save();
};

const assignTruckToUser = async (truckId, user) => {
  const assignedTrucks = await Truck.find({ assigned_to: user._id});
  if (assignedTrucks) {
    throw new Error(`You can't assign more than one truck`);
  }
  await Truck.findOneAndUpdate({ _id: truckId}, {$set: { assigned_to: user._id }});
};

const updateTruckInfoByIdForUser = async (truckId, user, data) => {
  await Truck.findOneAndUpdate({ _id: truckId, created_by: user._id }, {$set: data});
};

const deleteTruckByIdForUser = async (truckId, user) => {
  await Truck.findOneAndRemove({_id: truckId, created_by: user._id});
};


module.exports = {
  getTrucksByUserId,
  addTruckForUser,
  getTruckByIdForUser,
  updateTruckInfoByIdForUser,
  deleteTruckByIdForUser,
  assignTruckToUser,
};
