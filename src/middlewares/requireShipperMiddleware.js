const { User } = require('../models/userModel');

const requireShipperMiddleware = async (req, res, next) => {
  const user = await User.findOne({_id: req.userId});

  if (user.role !== 'SHIPPER') {
    res.status(403).json({ message: 'Sorry, only shipper can see page' });
  }

  req.user = user;
  next();
};

module.exports = {
  requireShipperMiddleware,
};
