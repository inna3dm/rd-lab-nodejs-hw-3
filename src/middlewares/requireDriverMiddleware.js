const { User } = require('../models/userModel');

const requireDriverMiddleware = async (req, res, next) => {
  const user = await User.findOne({_id: req.userId});

  if (user.role !== 'DRIVER') {
    res.status(403).json({ message: 'Sorry, only driver can see page' });
  }

  req.user = user;
  next();
};

module.exports = {
  requireDriverMiddleware,
};
