const express = require('express');
const config = require('config');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();
const nodeEnv = process.env.NODE_ENV || 'development';
const port = process.env.PORT || 8080;
const dbUrl = config.get(`${nodeEnv}.db.url`);

const { authRouter } = require('./controllers/authController');
const { usersRouter } = require('./controllers/usersController');
const { trucksRouter } = require('./controllers/trucksController');
const { loadsRouter } = require('./controllers/loadsController');
const { authMiddleware } = require('./middlewares/authMiddleware');
const { requireDriverMiddleware } = require('./middlewares/requireDriverMiddleware');
const { CustomError } = require('./utils/errors');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', [authMiddleware], usersRouter);
app.use('/api/trucks', [authMiddleware, requireDriverMiddleware], trucksRouter);
app.use('/api/loads', [authMiddleware], loadsRouter);

app.use((req, res, next) => {
  res.status(400).json({message: 'Not found'});
});

app.use((err, req, res, next) => {
  if (err instanceof CustomError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(400).json({message: err.message});
});


const start = async () => {
  try {
    await mongoose.connect(dbUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
