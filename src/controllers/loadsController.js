const express = require('express');
const router = express.Router();
const { User } = require('../models/userModel');

const {
  addLoadForUser,
  getLoadByIdForUser,
  getLoadAssignedToUser,
  updateLoadStateAssignedToUser,
  updateLoadByIdForUser,
  deleteLoadByIdForUser,
} = require('../services/loadsService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.post('/', asyncWrapper(async (req, res) => {
  const requiredRole = 'SHIPPER';
  const user = await User.findOne({_id: req.userId});
  if (requiredRole !== user.role) {
    res.status(403).json({ message: `Sorry, only ${requiredRole} can see page` });
  }

  await addLoadForUser(req.body, user);
  res.json({message: 'Load created successfully'});
}));



router.get('/:id', asyncWrapper(async (req, res) => {
  const requiredRole = 'SHIPPER';
  const user = await User.findOne({_id: req.userId});
  if (requiredRole !== user.role) {
    res.status(403).json({ message: `Sorry, only ${requiredRole} can see page` });
  }

  const { id } = req.params;
  const { load } = await getLoadByIdForUser(id, user);
  res.json({ load });
}));

router.get('/active', asyncWrapper(async (req, res) => {
  const requiredRole = 'DRIVER';
  const user = await User.findOne({_id: req.userId});
  if (requiredRole !== user.role) {
    res.status(403).json({ message: `Sorry, only ${requiredRole} can see page` });
  }

  const { load } = await getLoadAssignedToUser(user);
  load !== null ? res.json({ load }) : res.json({ message: "Driver doesn't have active loads" });
}));

router.patch('/active/state', asyncWrapper(async (req, res) => {
  const requiredRole = 'DRIVER';
  const user = await User.findOne({_id: req.userId});
  if (requiredRole !== user.role) {
    res.status(403).json({ message: `Sorry, only ${requiredRole} can see page` });
  }

  const { status } = await updateLoadStateAssignedToUser(user);
  res.json({message: `Load state changed to ${status}`})
}));

router.put('/:id', asyncWrapper(async (req, res) => {
  const requiredRole = 'SHIPPER';
  const user = await User.findOne({_id: req.userId});
  if (requiredRole !== user.role) {
    res.status(403).json({ message: `Sorry, only ${requiredRole} can see page` });
  }

  const { id } = req.params;
  await updateLoadByIdForUser(id, user, req.body);
  res.json({ message: 'Load details changed successfully' });
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const requiredRole = 'SHIPPER';
  const user = await User.findOne({_id: req.userId});
  if (requiredRole !== user.role) {
    res.status(403).json({ message: `Sorry, only ${requiredRole} can see page` });
  }

  const { id } = req.params;
  await deleteLoadByIdForUser(id, user);
  res.json({ message: 'Load deleted successfully' });
}));

module.exports = {
  loadsRouter: router,
};
