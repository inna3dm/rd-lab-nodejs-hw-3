const express = require('express');
const router = express.Router();

const {
  getUserProfile,
  changeUserPassword,
  deleteUserProfile,
} = require('../services/usersService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const user = await getUserProfile(userId);

  if (!user) {
    throw new Error('No user found!');
  }

  res.json({user});
}));

router.patch('/password', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await changeUserPassword(userId, req.body);

  res.json({message: 'Password changed successfully'});
}));

router.delete('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteUserProfile(userId);

  res.json({message: 'Profile deleted successfully'});
}));

module.exports = {
  usersRouter: router,
};
