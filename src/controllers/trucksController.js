const express = require('express');
const router = express.Router();

const {
  getTrucksByUserId,
  addTruckForUser,
  getTruckByIdForUser,
  updateTruckInfoByIdForUser,
  deleteTruckByIdForUser,
  assignTruckToUser
} = require('../services/trucksService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const user = req.user;
  const trucks = await getTrucksByUserId(user);
  res.json(trucks);
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const user = req.user;
  const { truckId } = req.params;
  const truck = await getTruckByIdForUser(truckId, user);
  res.json({ truck });
}));

router.post('/', asyncWrapper(async (req, res) => {
  const user = req.user;
  await addTruckForUser(req.body, user);
  res.json({message: 'Truck created successfully'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
  const user = req.user;
  const { truckId } = req.params;
  await assignTruckToUser(truckId, user);
  res.json({ message: 'Truck assigned successfully' });
}));

router.put('/:id', asyncWrapper(async (req, res) => {
  const user = req.user;
  const { truckId } = req.params;
  await updateTruckInfoByIdForUser(truckId, user, req.body);
  res.json({ message: 'Truck details changed successfully' });
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const user = req.user;
  const { truckId } = req.params;
  await deleteTruckByIdForUser(truckId, user);
  res.json({message: 'Truck deleted successfully'});
}));

module.exports = {
  trucksRouter: router,
};
