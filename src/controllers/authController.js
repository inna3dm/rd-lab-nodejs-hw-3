const express = require('express');
const router = express.Router();

const {
  registration,
  login,
  forgotPassword
} = require('../services/authService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.post('/register', asyncWrapper(async (req, res) => {
  const {
    email,
    password,
    role,
  } = req.body;

  await registration({email, password, role});

  res.json({message: 'Profile created successfully'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
  const {
    email,
    password,
  } = req.body;

  const token = await login({email, password});

  res.json({jwt_token: token});
}));

router.post('/forgot_password', asyncWrapper(async (req, res) => {
  const {
    email,
  } = req.body;

  res.json({message: 'New password sent to your email address'});
}));

module.exports = {
  authRouter: router,
};
