const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: String,
  },

  assigned_to: {
    type: String,
  },

  type: {
    type: String,
    uppercase: true,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },

  status: {
    type: String,
    enum: ['IS', 'OL'],
    default: 'IS',
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = { Truck };
