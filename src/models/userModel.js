const mongoose = require('mongoose');

const User = mongoose.model('User', {
  email: {
    type: String,
    unique: true,
    required: true,
  },

  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    required: true,
    uppercase: true,
  },

  password: {
    type: String,
    required: true,
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {User};
