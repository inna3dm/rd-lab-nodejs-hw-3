const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  name: {
    type: String,
    required: true,
  },

  created_by: {
    type: String,
  },

  assigned_to: {
    type: String,
  },

  status: {
    type: String,
    default: 'NEW',
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },

  state: {
    type: String,
  },

  payload: {
    type: Number,
    required: true,
  },

  pickup_address: {
    type: String,
    required: true,
  },

  delivery_address: {
    type: String,
    required: true,
  },

  dimensions: {
    width: {
      type: Number,
      required: true,
    },

    length: {
      type: Number,
      required: true,
    },

    height: {
      type: Number,
      required: true,
    },
  },

  logs: [
    {
      message: {
        type: String,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    }
  ],

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = { Load };
